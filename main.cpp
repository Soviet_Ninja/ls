#include <dirent.h>
#include <getopt.h>
#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#include <iostream>

void showls( struct stat *p, char *d_name ) {
   char flag;
   if ( S_ISREG( p->st_mode ) )
      flag = '-';
   else if ( S_ISDIR( p->st_mode ) )
      flag = 'd';
   else if ( S_ISCHR( p->st_mode ) )
      flag = 'c';
   else if ( S_ISBLK( p->st_mode ) )
      flag = 'b';
   else if ( S_ISSOCK( p->st_mode ) )
      flag = 's';
   else if ( S_ISFIFO( p->st_mode ) )
      flag = 'P';
   else if ( S_ISLNK( p->st_mode ) )
      flag = 'l';
   else
      flag = '?';
   //   std::cout << flag;
   printf( "%c\t", flag );

   printf( "%o\t", p->st_mode & 0777 );
   const char *rwx = "rwx";
   int i;
   for ( i = 0; i < 9; i++ ) printf( "%c", p->st_mode & 0400 ? rwx[i % 3] : '-' );
   printf( "\t" );

   printf( "%2d\t", p->st_nlink );

   struct passwd *s;
   s = getpwuid( p->st_uid );
   printf( "%7s\t", s->pw_name );

   printf( "%7s\t", getgrgid( p->st_gid )->gr_name );

   printf( "%ld\t", p->st_size );

   char buf[100];
   time_t t = p->st_mtime;
   strftime( buf, sizeof( buf ), "%F %T", localtime( &t ) );
   printf( "%s\t", buf );

   printf( "%s\t", d_name );
   printf( "\n" );
}

int main( int argc, char *argv[] ) {
#if 0
   char *path;
   if ( argc == 1 ) {
      path = ".";
   }

   if ( argc == 2 && strcmp( argv[1], "-l" ) == 0 ) {
      path = ".";
   }

   else if ( argc == 2 ) {
      path = argv[1];
   }

   if ( argc == 3 ) {
      path = argv[2];
   }

   if ( argc >= 2 && strcmp( argv[1], "-l" ) == 0 )
      printf( "Type\tPermission\t\tNumber of links\tUser name\tGroup\tNumber of bytes\tLast modification time\tFile name\n" );
   if ( opendir( path ) == NULL ) {  // Check if there is this directory
      printf( "File opening failed! Please enter it correctly! \n" );
      printf( "%m" );
      return -1;
   }
   DIR *p = opendir( path );                 // The pointer points to all the contents of the current path
                                             //   if ( chdir( path ) != 0 ) {  // Set path as the current working directory
                                             //      perror( "error" );
                                             //      return -1;
                                             //   }
   struct dirent *q;                         // The structure is divided into content blocks
   while ( ( q = readdir( p ) ) != NULL ) {  // Read from working directory
      if ( q->d_name[0] == '.' ) continue;   // Do not show hidden files

      if ( argc >= 2 && strcmp( argv[1], "-l" ) == 0 ) {
         struct stat s;
         stat( q->d_name, &s );
         showls( &s, q->d_name );
      }
      if ( argc == 1 ) printf( "%s\t", q->d_name );
   }
   printf( "\n" );
   closedir( p );
   return 0;
#else
   //   std::cout << "argc : " << argc << std::endl;
   //   for ( int i = 0; i < argc; ++i ) {
   //      std::cout << "argv[" << i << "] : " << argv[i] << std::endl;
   //   }

   auto opt = getopt( argc, argv, "-l" );
   std::cout << "opt : " << opt << std::endl;
#endif
}
